# Product API with Restify & MongoDB

#### Authentication
The Authentication uses a simple `Basic Authentication Header` using username and password.

Example sending request with authentidation header using curl:
```
curl -i -X GET "http://localhost:3000/products" -H "Content-Type: application/json" --user demo:cobaaja
```

#### Resource Endpoint

##### 1. Create Product
- method : `POST`
- endpoint : `/products`
- parameters:
  - name : String
  - description : String
  - category : String
  - price : Number
  - size : String
  - color : String
- request example:
  - `$ curl -i -X POST "http://localhost:3000/products" -H "Content-Type: application/json" 
     -d '{"name" : "Product1", "description": "Description1", price:10000}' --user demo:cobaaja`
  
##### 2. List Product
- method : `GET`
- endpoint : `/products`
- request example:
  - `$ curl -i -X GET "http://localhost:3000/products" -H "Content-Type: application/json" --user demo:cobaaja`

##### 3. Update Product
- method : `PUT`
- endpoint : `/products/:product_id`
- parameters:
  - name : String
  - description : String
  - category : String
  - price : Number
  - size : String
  - color : String
- request example:
  - `$ curl -i -X PUT "http://localhost:3000/products/5c14c2230b8eb86c6a424835" -H "Content-Type: application/json" 
     -d '{"size" : "big", "color" : "blue"}'`

  
##### 4. Delete Product
- method : `DELETE`
- endpoint : `/products/:product_id`
- request example:
  - `$ curl -i -X DELETE "http://localhost:3000/products/5c14c2230b8eb86c6a424835" -H "Content-Type: application/json"`
  
  
##### 5. Filter Product by size, color, & price range (Case study)
- method : `GET`
- endpoint : `/products`
- filter parameters:
  - size : String
  - color : String
  - price : Number
- request example:
  - `$ curl -i -X GET "http://localhost:3000/products?size=big&color=blue&price={gte}6000{lte}10000" -H "Content-Type: application/json"`
